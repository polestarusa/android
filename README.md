#NAO SDK Demo Application

* A sample application showcasing the integration of NAO SDK.
* It's provided as an Android Studio project.
* You can check out NAO Location, NAO Geofencing services in emulator mode straight out of the box 
* ... or use your own API Key to test it on your local deployment.

In addition, this application provides an implementation of the approach needed for Android 6.0 to check and request permissions at runtime.

Please see how checkPermissions() is used in com.polestar.examples.AbstractClient (method checkAndRequest() is declared in com.polestar.naosdkdemo.utils.PermissionsUtils)

NAO SDK Demo in Android studio:

![project_structure.PNG](https://bitbucket.org/repo/5rRAML/images/4220394365-project_structure.PNG)

Once running on your device, it should look like this:

* Left is the menu screen where you optionnally enter an API Key and select a service to run.
* Right is the Location service screen

![app_demo500px.png](https://bitbucket.org/repo/5rRAML/images/2534913361-app_demo500px.png)

The class diagram below shows the main components of the Demo application.

![class_demoapp.jpg](https://bitbucket.org/repo/5rRAML/images/2208288993-class_demoapp.jpg)