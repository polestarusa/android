package com.polestar.naosdkdemo;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.polestar.naosdk.api.external.NAOException;
import com.polestar.naosdk.api.external.NAOWakeUpRegistrationListener ;
import com.polestar.naosdk.api.external.NAOServicesConfig;
import com.polestar.naosdk.api.external.TNAOWAKEUP_REGISTER_STATUS;
import com.polestar.naosdkdemo.advanced.geofencingonsite.GeofencingOnSiteWakeUpNotifier;
import com.polestar.naosdkdemo.examples.AnalyticsClient;
import com.polestar.naosdkdemo.examples.BeaconProximityClient;
import com.polestar.naosdkdemo.examples.BeaconReportingClient;
import com.polestar.naosdkdemo.examples.GeofencingClient;
import com.polestar.naosdkdemo.examples.LocationInBackground;
import com.polestar.naosdkdemo.examples.LocationOnMap;
import com.polestar.naosdkdemo.examples.PeopleTrackingClient;
import com.polestar.naosdkdemo.models.ServicesItem;
import com.polestar.naosdkdemo.models.UseCaseItem;
import com.polestar.naosdkdemo.utils.PermissionsUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ServicesActivity extends AppCompatActivity {

    public static final String PREF_API_KEY = "[naoappdemo]api_key";
    public static String PREFS_NAME = "naoappdemoprefs";

    private ExpandableListView expandableServicesList;
    private AutoCompleteTextView apikeyEditTxt;
    private String[] keysFromAsset ;
    private int clickCounter = 0 ;


    private static final Map<String, String[]> USECASE_MENU = new LinkedHashMap<String, String[]>();
    static
    {
        USECASE_MENU.put("Location", new String[] {
                "On Map",
                "In Background"
        } );

        USECASE_MENU.put("Geofencing", new String[] {
        } );

        USECASE_MENU.put("Beacon Proximity", new String[] {
        } );

        USECASE_MENU.put("Analytics", new String[] {
        } );

        USECASE_MENU.put("People Tracking", new String[] {
        } );

        USECASE_MENU.put("Beacon Reporting", new String[] {
        } );

        USECASE_MENU.put("Advanced", new String[] {
                "Smart Park",
                "Geofencing Wake-up on-Site"
        } );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);

        //ask user for on-site wake-up feature
//        showOnSiteWakeUpDialog();

        //display software version
        TextView soft_version = (TextView)findViewById(R.id.software_version);
        soft_version.setText(NAOServicesConfig.getSoftwareVersion());


        //Init services list
        List<ServicesItem> servicesItemList = new ArrayList<>();
        for (String usecase: USECASE_MENU.keySet()) {
            String[] subUsecases = USECASE_MENU.get(usecase);
            ServicesItem usecaseItem = new ServicesItem(usecase);
            for (int i = 0; i < subUsecases.length ; i++){
                usecaseItem.getChildItemList().add(new UseCaseItem(subUsecases[i]));
            }
            servicesItemList.add(usecaseItem);
        }

        //Get the expandable list view and set adapter
        expandableServicesList = (ExpandableListView) findViewById(R.id.expandable_list);
        ExpandableListAdapter adapter = new ExpandableListAdapter(getApplicationContext(), servicesItemList);
        expandableServicesList.setAdapter(adapter);

        //Launch activity when user click on an sub item
        expandableServicesList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                    return launchCorrespondingActivity(groupPosition, childPosition);
            }
        });

        expandableServicesList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return launchCorrespondingActivity(groupPosition, -1);
            }
        });


        //Init the custom toolbar used to set the API Key
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        apikeyEditTxt = (AutoCompleteTextView) findViewById(R.id.searchEditText);
        apikeyEditTxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //change api key if user press set
                    setKeyToPrefs(apikeyEditTxt.getText().toString());
                    expandableServicesList.requestFocus();
                }
                //hide keyboard
                return false;
            }
        });

        apikeyEditTxt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus)
                    clickCounter = 0 ;
            }
        });
        apikeyEditTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                apikeyEditTxt.showDropDown();

                clickCounter++;
                if(clickCounter == 7){
                    try {
                        clickCounter = 0;
                        NAOServicesConfig.uploadNAOLogInfo("");
                        Toast.makeText(getApplicationContext(), "NAO Log info sent..", Toast.LENGTH_LONG).show();
                    } catch (NAOException e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                }
            }
        });
        setSupportActionBar(toolbar);

        //load keys from naoapi.key file
        initKeysFromAssets();
        if(keysFromAsset != null){
            ArrayAdapter<String> keysAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, keysFromAsset);
            apikeyEditTxt.setAdapter(keysAdapter);
        }
        String keyDisplayed = getKeyFromPrefs(getApplicationContext());
        //init with pref if exist
        if(!keyDisplayed.isEmpty()){
            apikeyEditTxt.setText(keyDisplayed);
        }
        else if(keysFromAsset != null){
            //init key from file if exists
            apikeyEditTxt.setText(keysFromAsset[0]);
            setKeyToPrefs(keysFromAsset[0]);
        }
    }

    @Override
    public void onBackPressed() {
        showOnSiteWakeUpDialog();
    }

    /**
     * Launch activity corresponding to position click in expandable list view
     * @param group
     * @param child
     * @return
     */
    private boolean launchCorrespondingActivity(int group, int child){
        Intent intent;
        switch(group){
            case 0: //Location services
                switch(child){
                    case 0:
                        intent = new Intent(getApplicationContext(), LocationOnMap.class);
                        startActivity(intent);
                        return true;
                    case 1:
                        intent = new Intent(getApplicationContext(), LocationInBackground.class);
                        startActivity(intent);
                        return true;
                    default:
                        return false;
                }
            case 1: //Geofencing
                intent = new Intent(getApplicationContext(), GeofencingClient.class);
                startActivity(intent);
                return true;
            case 2: //Beacon Proximity
                intent = new Intent(getApplicationContext(), BeaconProximityClient.class);
                startActivity(intent);
                return true;
            case 3: // Analytics
                intent = new Intent(getApplicationContext(), AnalyticsClient.class);
                startActivity(intent);
                return true;
            case 4: // Tracking
                intent = new Intent(getApplicationContext(), PeopleTrackingClient.class);
                startActivity(intent);
                return true;
            case 5: // Beacon Reporting
                intent = new Intent(getApplicationContext(), BeaconReportingClient.class);
                startActivity(intent);
                return false;
            case 6: // Smart park
                switch(child){
                    case 0:
                        intent = new Intent(getApplicationContext(), com.polestar.naosdkdemo.advanced.smartpark.MainActivity.class);
                        startActivity(intent);
                        return true;
                    case 1:
                        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.P || PermissionsUtils.checkAndRequest(this, new ArrayList<>(Arrays.asList(Manifest.permission.ACCESS_BACKGROUND_LOCATION)),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Toast.makeText(getApplicationContext(), "Cannot run the service because ACCESS_BACKGROUND_LOCATION permissions have been denied", Toast.LENGTH_SHORT).show();
                                    }
                                })) {
                            startGeofencingOnSiteExample();
                        } else
                            Toast.makeText(getApplicationContext(), "Cannot run the service because ACCESS_BACKGROUND_LOCATION permissions have been denied", Toast.LENGTH_SHORT).show();
                        return true;
                    default:
                        return false;
                }
            default:
                return false;
        }
    }

    /**
     * Init key from file if exists and put it in shared preferences
     */
    public void initKeysFromAssets(){
        BufferedReader reader ;
        String keys ;
        try{
            final InputStream file = getAssets().open("naoapi.key");
            reader = new BufferedReader(new InputStreamReader(file));

            String line = reader.readLine();
            while(line != null){
                //check if line contains api key
                if(line.contains("keys=")){
                    keys = line.replaceFirst("keys=", "").trim();
                    if(keys.length() > 1){
                        //split keys into array
                        keysFromAsset = keys.split(",");
                        return;
                    }
                }
                line = reader.readLine();
            }


        } catch (IOException e) {
            Log.e(getClass().getName(), "Cannot read key from assets", e);
        }
    }

    /**
     * Save the given key in shared preferences
     * @param apiKey
     */
    public void setKeyToPrefs(String apiKey){
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit() ;
        editor.putString(PREF_API_KEY, apiKey.trim());
        editor.commit();

    }

    /**
     * Get current key saved
     * @param ctxt
     * @return
     */
    public static String getKeyFromPrefs(Context ctxt){
        return ctxt.getApplicationContext().getSharedPreferences(PREFS_NAME, 0).getString(PREF_API_KEY, "");
    }


    protected void startGeofencingOnSiteExample(){
        //Enable on-site wake-up for geofencing on site use case
        NAOServicesConfig.enableOnSiteWakeUp(this, getKeyFromPrefs(this), GeofencingOnSiteWakeUpNotifier.class, new NAOWakeUpRegistrationListener() {
            @Override
            public void onStatusChanged(TNAOWAKEUP_REGISTER_STATUS status, String message) {
                if (status == TNAOWAKEUP_REGISTER_STATUS.REGISTERED)
                    Toast.makeText(getBaseContext(), "SUCCESS: Geofencing On-Site Wake-Up enabled", Toast.LENGTH_LONG).show();
                else if (status == TNAOWAKEUP_REGISTER_STATUS.REGISTERED_GEOFENCES_EVENTS || status == TNAOWAKEUP_REGISTER_STATUS.REGISTERED_SITES_EVENTS)
                    Toast.makeText(getBaseContext(), "WARNING: " + message, Toast.LENGTH_LONG).show();
                else if (status == TNAOWAKEUP_REGISTER_STATUS.REGISTER_ERROR)
                    Toast.makeText(getBaseContext(), "ERROR: " + message + "\nMake sure you performed synchronize data", Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(getBaseContext(), "ERROR: Geofencing On-Site Wake-Up registration.\nUnknown issue: " + message, Toast.LENGTH_LONG).show();
            }
        });

    }

    protected void disableOnSiteWakeUp(){
        Context ctxt = getApplicationContext();
        NAOServicesConfig.disableOnSiteWakeUp(ctxt, getKeyFromPrefs(ctxt), new NAOWakeUpRegistrationListener() {
            @Override
            public void onStatusChanged(TNAOWAKEUP_REGISTER_STATUS status, String message) {
                if(status == TNAOWAKEUP_REGISTER_STATUS.UNREGISTERED){
                    Toast.makeText(getBaseContext(), "On-site wake-up notifier unregister successful", Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(getBaseContext(), "On-site wake-up notifier unregister failed", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /**
     * Ask user to activate or not "on-site wake-up" feature
     */
    public void showOnSiteWakeUpDialog(){
        new android.app.AlertDialog.Builder(this)
                .setTitle("On-site wake-up activation")
                .setMessage("Do you want to be notified when you are on site even if the app has been killed ?")
                .setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // do nothing if on-site wake-up feature has been already activated
                                finish();

                            }
                        })
                .setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                disableOnSiteWakeUp();
                                finish();
                            }
                        })
                .show();
    }
}
