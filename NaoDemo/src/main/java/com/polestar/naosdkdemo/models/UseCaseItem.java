package com.polestar.naosdkdemo.models;


public class UseCaseItem {
    private String name;

    public UseCaseItem(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
