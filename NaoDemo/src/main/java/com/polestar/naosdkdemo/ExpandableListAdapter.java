package com.polestar.naosdkdemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.polestar.naosdkdemo.models.ServicesItem;
import com.polestar.naosdkdemo.models.UseCaseItem;

import java.util.List;

/**
 * List adapter used in ServiceActivity
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private static final class ViewHolder {
        TextView textLabel;
    }

    private final List<ServicesItem> servicesItemList;

    private final LayoutInflater inflater ;

    public ExpandableListAdapter(Context context, List<ServicesItem> servicesList){
        this.inflater = LayoutInflater.from(context);
        this.servicesItemList = servicesList;

    }


    @Override
    public UseCaseItem getChild(int groupPosition, int childPosition) {
        return servicesItemList.get(groupPosition).getChildItemList().get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return servicesItemList.get(groupPosition).getChildItemList().size();
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View resultView = convertView;
        ViewHolder holder ;

        if(resultView == null){
            resultView = inflater.inflate(R.layout.child_item, null);
            holder = new ViewHolder();
            holder.textLabel = (TextView) resultView.findViewById(R.id.child_txtview);
            resultView.setTag(holder);
        } else {
            holder = (ViewHolder) resultView.getTag();
        }

        final UseCaseItem item = getChild(groupPosition, childPosition);
        holder.textLabel.setText(item.getName());

        return resultView;
    }

    @Override
    public ServicesItem getGroup(int groupPosition) {
        return servicesItemList.get(groupPosition);
    }
    @Override
    public int getGroupCount() {
        return servicesItemList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View resultView = convertView;
        ViewHolder holder;

        if (resultView == null) {
            resultView = inflater.inflate(R.layout.service_list_item, null);
            holder = new ViewHolder();
            holder.textLabel = (TextView) resultView.findViewById(R.id.parent_txtview);
            resultView.setTag(holder);
        } else {
            holder = (ViewHolder) resultView.getTag();
        }

        final ServicesItem item = getGroup(groupPosition);

        holder.textLabel.setText(item.getName());

        return resultView;
    }



    @Override
    public boolean hasStableIds() {
        return true;
    }



    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
