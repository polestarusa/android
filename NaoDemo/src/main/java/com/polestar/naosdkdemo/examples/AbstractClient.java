package com.polestar.naosdkdemo.examples;

import android.Manifest;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.polestar.naosdk.api.external.NAOERRORCODE;
import com.polestar.naosdk.api.external.NAOSensorsListener;
import com.polestar.naosdk.api.external.NAOServiceHandle;
import com.polestar.naosdk.api.external.NAOSyncListener;
import com.polestar.naosdkdemo.R;
import com.polestar.naosdkdemo.ServicesActivity;
import com.polestar.naosdkdemo.utils.ColorUtils;
import com.polestar.naosdkdemo.utils.NotificationsUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Generic class holding functionality common to all service clients.
 * In particular, it implements NAOSensorsListener to receive notification about sensor requirements
 * needed by a particular service (for example: Bluetooth activation).
 */
public abstract class AbstractClient<ServiceHandle extends NAOServiceHandle> extends AppCompatActivity implements NAOSensorsListener, View.OnClickListener{

    protected static final String EMULATOR_KEY = "emulator";

    protected ServiceHandle serviceHandle; // generic service handle
    protected String apiKeyUsed ;

    /**
     * ===== SENSORS LISTENER CALLBACKS  =======
     */

    @Override
    public void requiresCompassCalibration() { notifyUser("please calibrate Compass"); }

    @Override
    public void requiresWifiOn() {
        notifyUser("please turn on Wifi");
    }

    @Override
    public void requiresBLEOn() {
        notifyUser("please turn on Bluetooth");
    }

    @Override
    public void requiresLocationOn() {
        notifyUser("please turn on Location");
    }

    /**
     * ===== PERMISSION CHECKING =======
     */

    protected void checkPermissions(){
        locationPermissionRequest.launch(new String[] {
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        });
    }

    ActivityResultLauncher<String[]> locationPermissionRequest =
            registerForActivityResult(new ActivityResultContracts
                            .RequestMultiplePermissions(), result -> {
                        Boolean fineLocationGranted = result.getOrDefault(
                                Manifest.permission.ACCESS_FINE_LOCATION, false);

                        if (fineLocationGranted != null && fineLocationGranted) {
                            Log.d(getClass().getSimpleName(), "Android12 locationPermissionRequest >> fineLocationGranted is true");
                            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.R) {
                                requestBTPermissions();
                            }
                            else startService();
                        }
                    }
            );

    private void requestBTPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.BLUETOOTH_SCAN)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setCancelable(false)
                        .setMessage("Permissions management")
                        .setPositiveButton(this.getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                defaultPermissionRequest.launch(new String[]{
                                        Manifest.permission.BLUETOOTH_SCAN
                                });
                            }
                        })
                        .setNegativeButton(this.getResources().getString(android.R.string.cancel), null)
                        .show();

            } else {
                defaultPermissionRequest.launch(new String[]{
                        Manifest.permission.BLUETOOTH_SCAN
                });
            }
        }
    }

    ActivityResultLauncher<String[]> defaultPermissionRequest =
            registerForActivityResult(new ActivityResultContracts
                            .RequestMultiplePermissions(), result -> {
                        Boolean btGranted = result.getOrDefault(Manifest.permission.BLUETOOTH_SCAN, false);
                        if (btGranted != null && btGranted) {
                            startService();
                        }
//                        else
//                            onPermissionsRefused();
                    }
            );

    private void onPermissionsRefused(){
        notifyUser("Cannot run the service because permissions have been denied");
        setNaoServiceStarted(false);

        startButton.setEnabled(false);
        syncButton.setEnabled(false);
        startButton.setBackgroundColor(ColorUtils.getColor(this, R.color.disabledButton));
        syncButton.setBackgroundColor(ColorUtils.getColor(this, R.color.disabledButton));
        syncStateTextView.setText("No sync in progress");
    }

    /**
     * ===== NAO Data Synchronization =======
     */

    protected NAOSyncListener naoSyncListener;

    protected void synchronizeData() {

        if(null == serviceHandle){
            createHandle();
        }

        syncStateTextView.setText("Sync in progress");
        serviceHandle.synchronizeData(naoSyncListener);
    }


    /**
     * ===== ACTIVITY LIFECYCLE and UI =======
     */

    protected boolean isServiceStarted;

    protected Button startButton;
    protected Button syncButton;
    protected Switch emulatorSwitch;
    protected TextView serviceStateTextView;
    protected TextView syncStateTextView;


    protected abstract void createHandle();

    protected abstract boolean hasEmulator();

    protected void startService() {

        //checkPermissions();
        // init service
        if (serviceHandle == null) {
            createHandle();
        }

        if(serviceHandle.start())
            setNaoServiceStarted(true);

    }

    protected void stopService() {
        if(serviceHandle != null)
            serviceHandle.stop();

        setNaoServiceStarted(false);
    }

    /**
    * ===== Activity lifecycle =====
    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        TextView title = (TextView)findViewById(R.id.service_title);
        title.setText("NAO"+getClass().getSimpleName());

        naoSyncListener = new NAOSyncListener() {

            @Override
            public void onSynchronizationSuccess() {
                Log.d(this.getClass().getName(), "onSynchronizationSuccess");
                notifyUser("onSynchronizationSuccess");
                syncStateTextView.setText("Sync succeeded");
            }

            @Override
            public void onSynchronizationFailure(NAOERRORCODE errorCode, String message) {
                notifyUser("onSynchronizationFailure: " + message);
                Log.d(this.getClass().getName(), "onSynchronizationFailure: " + message);
                syncStateTextView.setText("Sync failed");
            }
        };

        startButton = (Button) findViewById(R.id.startButton);
        syncButton = (Button) findViewById(R.id.syncButton);

        emulatorSwitch = (Switch) findViewById(R.id.emulator_switch);
        emulatorSwitch.setEnabled(hasEmulator());
        emulatorSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    apiKeyUsed = EMULATOR_KEY;
                } else {
                    apiKeyUsed = ServicesActivity.getKeyFromPrefs(getApplicationContext());
                }
                stopService();
                serviceHandle = null;
            }
        });

        serviceStateTextView = (TextView) findViewById(R.id.serviceState);
        syncStateTextView = (TextView) findViewById(R.id.syncState);

//        Init api key:
        apiKeyUsed = ServicesActivity.getKeyFromPrefs(getApplicationContext());
        if(apiKeyUsed.isEmpty() && hasEmulator()){
//           no api key configured: set emulator if available
            emulatorSwitch.setChecked(hasEmulator());
        }
        else{
            emulatorSwitch.setChecked(false);
        }

        startButton.setOnClickListener(this);
        startButton.setEnabled(true);
        startButton.setBackgroundColor(ColorUtils.getColor(this, R.color.enabledButton));

        syncButton.setOnClickListener(this);
        syncButton.setEnabled(true);
        syncButton.setBackgroundColor(ColorUtils.getColor(this, R.color.enabledButton));

        updateServiceStateTextView(false);// display text saying that the service is not started

        syncStateTextView.setText("No sync in progress");

        setNaoServiceStarted(false);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.startButton) {
            if (!isServiceStarted) {
                checkPermissions();
            } else {
                stopService();
            }
        }
        else if (v.getId() == R.id.syncButton) {
            synchronizeData();
        }
    }

    private void updateServiceStateTextView(boolean serviceStarted) {
        if (serviceStarted) {
            serviceStateTextView.setText(R.string.service_started);
        } else {
            serviceStateTextView.setText(R.string.service_stopped);
        }
    }

    /**
     * Display message on screen to user
     * @param msg text to display
     */
    public void notifyUser(String msg){
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    public void setNaoServiceStarted(boolean naoStarted) {
        startButton.setEnabled(true);
        if (naoStarted) {
            startButton.setText(getResources().getString(R.string.stop_button));
        } else {
            startButton.setText(getResources().getString(R.string.start_button));
        }

        if (naoStarted) {
            // Case where naoStarted==true is properly handled in
            // ServiceStarted, so that the message is displayed when the
            // service is actually started. We use the starting message here
            // instead.
            serviceStateTextView.setText(R.string.service_starting);
        } else {
            updateServiceStateTextView(naoStarted);
        }
        isServiceStarted = naoStarted;
    }


    public static void showNotification(Context ctxt, String title, String message, boolean setSound){
        String CHANNEL_ID = "my_channel_01";
        NotificationChannel mChannel = null;

        // initialize notification manager
        NotificationManager mNotificationManager =(NotificationManager) ctxt.getSystemService(Context.NOTIFICATION_SERVICE);
        // create notification channel for target since Android_SDK >= 26
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            mChannel = new NotificationChannel(CHANNEL_ID, ctxt.getString(R.string.channel_name),
                    mNotificationManager.IMPORTANCE_DEFAULT);
            mChannel.setDescription(ctxt.getString(R.string.channel_description));
            mNotificationManager.createNotificationChannel(mChannel);
        }

        //Get date
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();

        // display the notification
        mNotificationManager.notify(1, NotificationsUtils.createNotification(ctxt, title,
                dateFormat.format(date)+"\t"+message, android.R.drawable.ic_dialog_info, mChannel,
                true, null, null));
    }
}
