package com.polestar.naosdkdemo.examples;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.polestar.naosdk.api.external.TPOWERMODE;
import com.polestar.naosdkdemo.utils.PermissionsUtils;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Class showing how location service should be used when location in background is needed
 */
public class LocationInBackground extends AbstractLocationClient{


    /**
     * Enable emulator for this client
     */
    @Override
    protected boolean hasEmulator(){
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
        if(null != serviceHandle) {
            Log.d(getClass().getName(), "onResume -- POWER MODE HIGH");
            serviceHandle.setPowerMode(TPOWERMODE.HIGH);
        }
    }
    @Override
    public void onPause() {
        super.onPause();
        // Another activity is taking focus (this activity is about to be "paused").
        if(null != serviceHandle) {
            Log.d(getClass().getName(), "onPause -- POWER MODE LOW");
            serviceHandle.setPowerMode(TPOWERMODE.LOW);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
        if(null !=  serviceHandle){
            Log.d(getClass().getName(), "onDestroy -- stop location service");
            serviceHandle.stop();
            serviceHandle = null;
        }
    }
}
