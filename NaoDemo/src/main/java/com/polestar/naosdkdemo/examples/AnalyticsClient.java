package com.polestar.naosdkdemo.examples;

import android.util.Log;

import com.polestar.naosdk.api.external.NAOAnalyticsHandle;
import com.polestar.naosdk.api.external.NAOAnalyticsListener;
import com.polestar.naosdk.api.external.NAOERRORCODE;
import com.polestar.naosdkdemo.MyNaoService;

/**
 * Class showing how Analytics should be used for uses cases when background running  is needed
 */
public class AnalyticsClient extends AbstractClient<NAOAnalyticsHandle> implements NAOAnalyticsListener {


    @Override
    protected void createHandle()
    {
        serviceHandle = new NAOAnalyticsHandle(this, MyNaoService.class, apiKeyUsed, this, this);
    }

    @Override
    protected boolean hasEmulator() {
        return false;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
        if(null != serviceHandle){
            Log.d(getClass().getName(), "onDestroy -- stop location service");
            serviceHandle.stop();
            serviceHandle = null;
        }
    }

    /**
     * ===== OVERRIDE LISTENER CALLBACKS =======
     */

    @Override
    public void onError(NAOERRORCODE errCode, String msg) {
        Log.e(this.getClass().getName(), "onError " + msg);
        notifyUser("onError " + errCode + " " + msg);
    }

    @Override
    public void onBleError(int i) {
        Log.e(this.getClass().getName(), "onBleError " + i);
    }
}
