package com.polestar.naosdkdemo.examples;

import android.util.Log;

import com.polestar.naosdk.api.external.NAOBeaconProximityHandle;
import com.polestar.naosdk.api.external.NAOBeaconProximityListener;
import com.polestar.naosdk.api.external.NAOERRORCODE;
import com.polestar.naosdk.api.external.TBEACONSTATE;
import com.polestar.naosdkdemo.MyNaoService;

/**
 * Generic class holding functionality common to all BeaconProximity service clients.
 * It creates service handle and implements NAOBeaconProximityListener to receive events about proximity change and
 * signal strength about a beacon.
 */
public class BeaconProximityClient extends AbstractClient<NAOBeaconProximityHandle> implements NAOBeaconProximityListener {


    @Override
    protected void createHandle()
    {
        serviceHandle = new NAOBeaconProximityHandle(this, MyNaoService.class, apiKeyUsed, this, this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Stop the service when the activity is about to be destroyed
        if(null != serviceHandle){
            serviceHandle.stop();
            serviceHandle = null;
        }
    }
    /**
     * Disable emulator mode for BeaconProximity
     */
    @Override
    protected boolean hasEmulator() {
        return false;
    }


    /**
     * ===== OVERRIDE LISTENER CALLBACKS =======
     */

    @Override
    public void onProximityChange(TBEACONSTATE beaconState, String label) {
        String prox;
        switch (beaconState){
            case FIRST_FAR:
                prox = "Far";
                break;
            case FIRST_NEAR:
                prox = "Near";
                break;
            case FIRST_UNSEEN:
                prox = "Unseen";
                break;
            default:
                prox = "Unknown";
        }
        notifyUser(label + " " + prox);
    }

    @Override
    public void onBeaconRange(int rssi, String label) {
        Log.d(this.getClass().getName(), "onBeaconRange " + label + " - " + rssi);
    }

    @Override
    public void onError(NAOERRORCODE errCode, String msg) {
        Log.e(this.getClass().getName(), "onError " + msg);
        notifyUser("onError " + errCode + " " + msg);
    }

    @Override
    public void onBleError(int i) {
        Log.e(this.getClass().getName(), "onBleError " + i);
    }
}
