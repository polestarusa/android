package com.polestar.naosdkdemo.examples;

import android.location.Location;
import android.util.Log;

import com.polestar.naosdk.api.external.NAOERRORCODE;
import com.polestar.naosdk.api.external.NAOLocationHandle;
import com.polestar.naosdk.api.external.NAOLocationListener;
import com.polestar.naosdk.api.external.TNAOFIXSTATUS;
import com.polestar.naosdkdemo.MyNaoService;

/**
 * Generic class holding functionality common to all Location service clients.
 * It creates service handle and implements NAOLocationListener to receive location and service status
 */
public abstract class AbstractLocationClient extends AbstractClient<NAOLocationHandle> implements NAOLocationListener {


    @Override
    protected void createHandle() {
        serviceHandle = new NAOLocationHandle(this, MyNaoService.class, apiKeyUsed, this, this);
    }

    /**
     * Enable emulator mode for Location
     */
    @Override
    protected boolean hasEmulator() {
        return true;
    }

    /**
     * ===== OVERRIDE LISTENER CALLBACKS =======
     */

    @Override
    public void onEnterSite(String s) {
        showNotification(getApplicationContext(),getClass().getSimpleName(), "onEnterSite", false);
    }

    @Override
    public void onExitSite(String s) {
        showNotification(getApplicationContext(),getClass().getSimpleName(), "onExitSite", false);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("dbg", location.getLatitude() + "," + location.getLongitude() + "," + location.getAltitude() + "," + location.getAccuracy() + "," + location.getBearing());
        serviceStateTextView.setText("LAT: "+location.getLatitude()
                + "\nLON: " + location.getLongitude()
                + "\nALT: " +location.getAltitude()
                + "\nBEA: " +location.getBearing());
    }

    @Override
    public void onLocationStatusChanged(TNAOFIXSTATUS status) {
        String msg = "";
        switch (status) {
            case NAO_FIX_AVAILABLE:
                Log.d("dbg", "status changed > AVAILABLE");
                msg = "NAOLocationHandle.AVAILABLE";
                Log.w("version pdb","pdb version:" +serviceHandle.getDatabaseVersions());
                break;
            case NAO_TEMPORARY_UNAVAILABLE:
                Log.d("dbg", "status changed > TEMPORARILY UNAVAILABLE");
                msg = "NAOLocationHandle.TEMPORARILY_UNAVAILABLE";
                serviceStateTextView.setText("");
                break;
            case NAO_OUT_OF_SERVICE:
            default:
                Log.d("dbg", "status changed > OUT_OF_SERVICE");
                msg = "NAOLocationHandle.OUT_OF_SERVICE";
                serviceStateTextView.setText("");
                break;
        }
        showNotification(getApplicationContext(),getClass().getSimpleName(), msg, false);
    }

    @Override
    public void onError(NAOERRORCODE errCode, String msg) {
        Log.e(this.getClass().getName(), "onError " + msg);
        notifyUser("onError " + errCode + " " + msg);
    }

    @Override
    public void onBleError(int i) {
        Log.e(this.getClass().getName(), "onBleError " + i);
    }
}
