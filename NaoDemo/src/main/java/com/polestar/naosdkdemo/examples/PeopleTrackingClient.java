package com.polestar.naosdkdemo.examples;

import android.util.Log;

import com.polestar.naosdk.api.external.NAOAnalyticsHandle;
import com.polestar.naosdk.api.external.NAOAnalyticsListener;
import com.polestar.naosdk.api.external.NAOERRORCODE;
import com.polestar.naosdk.api.external.NAOException;
import com.polestar.naosdk.api.external.NAOServicesConfig;
import com.polestar.naosdk.api.external.NAOTrackingHandle;
import com.polestar.naosdk.api.external.NAOTrackingListener;
import com.polestar.naosdkdemo.MyNaoService;

/**
 * Class showing how tracking service should be used for uses cases when background running  is needed
 */
public class PeopleTrackingClient extends AbstractClient<NAOTrackingHandle> implements NAOTrackingListener {


    @Override
    protected void createHandle()
    {
        serviceHandle = new NAOTrackingHandle(this, MyNaoService.class, apiKeyUsed, this, this);
        try {
            //you need to set your tracking id 
            NAOServicesConfig.setIdentifier(this,"tracking_identifer",true);
        } catch (NAOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected boolean hasEmulator() {
        return false;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
        if(null != serviceHandle){
            Log.d(getClass().getName(), "onDestroy -- stop location service");
            serviceHandle.stop();
            serviceHandle = null;
        }
    }

    /**
     * ===== OVERRIDE LISTENER CALLBACKS =======
     */

    @Override
    public void onError(NAOERRORCODE errCode, String msg) {
        Log.e(this.getClass().getName(), "onError " + msg);
        notifyUser("onError " + errCode + " " + msg);
    }

    @Override
    public void onBleError(int i) {
        Log.e(this.getClass().getName(), "onBleError " + i);
    }

    @Override
    public void onDeliveryCompleted() {
        Log.w(this.getClass().getName(), "Tracking: onDeliveryCompleted");
    }
}
