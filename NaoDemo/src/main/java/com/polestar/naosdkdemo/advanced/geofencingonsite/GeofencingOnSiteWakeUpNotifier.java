package com.polestar.naosdkdemo.advanced.geofencingonsite;

import android.content.Intent;
import android.os.Build;

import com.polestar.helpers.Log;
import com.polestar.naosdk.api.external.NAOWakeUpNotifier;

/**
 * Class called when user enter/leave the GPS geofence area defined in NAOCloud
 */
public class GeofencingOnSiteWakeUpNotifier extends NAOWakeUpNotifier{


    @Override
    public void onEnterBeaconArea() {
        //In area covered by beacon. Called only if application has been killed
    }

    @Override
    public void onEnterGPSArea(){
        //In GPS area
        // Start service
        Log.alwaysWarn(this.getClass().getName(),">> onEnterGPSArea");
        Intent intent = new Intent(getContext(), GeofencingService.class);
        // start service depending on Android_SDK version
        if (Build.VERSION.SDK_INT >= 26){
            getContext().startForegroundService(intent);
        }
        else getContext().startService(intent);
    }


    @Override
    public void onExitGPSArea(){
        //stop service
        Log.alwaysWarn(this.getClass().getName(), ">> onExitGPSArea");
        Intent intent = new Intent(getContext(), GeofencingService.class);
        getContext().stopService(intent);
    }
}
