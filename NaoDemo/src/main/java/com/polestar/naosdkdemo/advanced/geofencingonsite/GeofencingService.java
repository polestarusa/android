package com.polestar.naosdkdemo.advanced.geofencingonsite;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.polestar.helpers.Log;
import com.polestar.naosdk.api.external.NAOERRORCODE;
import com.polestar.naosdk.api.external.NAOGeofencingHandle;
import com.polestar.naosdk.api.external.NAOGeofencingListener;
import com.polestar.naosdk.api.external.NAOSensorsListener;
import com.polestar.naosdk.api.external.NAOSyncListener;
import com.polestar.naosdk.api.external.NaoAlert;
import com.polestar.naosdk.api.external.TPOWERMODE;
import com.polestar.naosdkdemo.MyNaoService;
import com.polestar.naosdkdemo.R;
import com.polestar.naosdkdemo.ServicesActivity;
import com.polestar.naosdkdemo.utils.NotificationsUtils;


public class GeofencingService extends Service implements NAOGeofencingListener, NAOSensorsListener {

    private String TAG = getClass().getSimpleName();
    private NAOGeofencingHandle naoGeofencingHandle;
    private static final int NOTIF_ID = 1;
    private final String CHANNEL_ID = "my_channel_01";
    private NotificationChannel mChannel = null;
    private NotificationManager mNotificationManager;

    @Override
    public void onCreate() {
        // create service handle
        naoGeofencingHandle = new NAOGeofencingHandle(this, MyNaoService.class, ServicesActivity.getKeyFromPrefs(getApplicationContext()), this, this);
        // set power mode to low because service is used in background
        naoGeofencingHandle.setPowerMode(TPOWERMODE.LOW);
        // initialize notification manager
        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        // create notification channel for target since Android_SDK >= 26
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            mChannel = new NotificationChannel(CHANNEL_ID, getString(R.string.channel_name),
                    mNotificationManager.IMPORTANCE_DEFAULT);
            mChannel.setDescription(getString(R.string.channel_description));
            mNotificationManager.createNotificationChannel(mChannel);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // start the service and create the notification associated
        startForeground(NOTIF_ID, NotificationsUtils.createNotification(this, "Pole Star",
                "GeofencingService running", android.R.drawable.ic_dialog_info, mChannel, false,
                null,   // null because no Pending intent is needed
                null)); // null because no actions are needed
        Log.alwaysWarn(getClass().getName(), "Service started");

        naoGeofencingHandle.synchronizeData(new NAOSyncListener() {
            @Override
            public void onSynchronizationSuccess() {
            }

            @Override
            public void onSynchronizationFailure(NAOERRORCODE errorCode, String message) {
            }
        });
        //start geofencing service
        if (naoGeofencingHandle.start())
            showNotification(TAG, "Started");

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        // The service is no longer used and is being destroyed
        if (naoGeofencingHandle != null) {
            naoGeofencingHandle.stop();
        }
        showNotification(TAG, "Service stopped");
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onError(NAOERRORCODE errCode, String message) {
        showNotification(TAG, "Error: " + errCode + " " + message);
    }

    @Override
    public void onFireNaoAlert(NaoAlert alert) {
        showNotification(TAG, alert.getName());
    }

    public void showNotification(String title, String message) {
        // display the notification
        mNotificationManager.notify(0, NotificationsUtils.createNotification(this, title, message,
                android.R.drawable.ic_dialog_info, mChannel, true, null, null));
    }

    /**
     * Ask user to turn on Bluetooth sensor
     */
    private void showNotificationBluetoothSetting() {
        // set the intent to enable bluetooth when click on the notification
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        PendingIntent pi = PendingIntent.getActivity(this, 0, enableBtIntent, PendingIntent.FLAG_IMMUTABLE);

        // display the notification
        mNotificationManager.notify(1, NotificationsUtils.createNotification(this, TAG,
                "Please enable Bluetooth", android.R.drawable.ic_dialog_info, mChannel, true, pi,
                null));
    }

    /**
     * Ask user to turn on location sensor
     */
    private void showNotificationLocationSetting() {
        // set the intent to go to location settings when click on the notification
        Intent enablelocIntent=new Intent("android.settings.LOCATION_SOURCE_SETTINGS");
        PendingIntent pi = PendingIntent.getActivity(this, 0, enablelocIntent, PendingIntent.FLAG_IMMUTABLE);

        // display the notification
        mNotificationManager.notify(1, NotificationsUtils.createNotification(this, TAG,
                "Please enable Location", android.R.drawable.ic_dialog_info, mChannel, true, pi,
                null));
    }

    @Override
    public void requiresCompassCalibration() {
    }

    @Override
    public void requiresWifiOn() {
        showNotification(TAG, "Enable Wifi");
    }

    @Override
    public void requiresBLEOn() {
        //ask user to enable BLE
        showNotificationBluetoothSetting();
    }

    @Override
    public void requiresLocationOn() {
        //ask user to enable Location
        showNotificationLocationSetting();
    }

    @Override
    public void onBleError(int i) {

    }
}
