//______________________________________________________________________________________
//
//  MyNaoLocationService
// 
//  Pole Star Confidential Proprietary
//    Copyright (c) Pole Star 2014, All Rights Reserved
//    No publication authorized. Reverse engineering prohibited
//______________________________________________________________________________________
package com.polestar.naosdkdemo;


import com.polestar.naosdk.managers.NaoServiceManager;

/**
 * Implementation of the abstract NaoServiceManager.
 */
public class MyNaoService extends NaoServiceManager {
}
