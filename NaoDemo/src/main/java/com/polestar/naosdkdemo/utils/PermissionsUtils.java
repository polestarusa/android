package com.polestar.naosdkdemo.utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Example of permission handling :
 * ask user to confirm permissions requested by the app.
 *
 * Note that since Android 6, it has become mandatory for the app to ask the end user explicitly
 */
public class PermissionsUtils {

    public static final int PERMISSION_ACCESS_BACKGROUND_LOC_REQUEST_CODE = 1;

    private static final Map<String, Pair<Integer, String>> DEFAULT_PERMISSIONS = new HashMap<String, Pair<Integer, String>>() {
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                put( Manifest.permission.ACCESS_BACKGROUND_LOCATION, new Pair<>(PermissionsUtils.PERMISSION_ACCESS_BACKGROUND_LOC_REQUEST_CODE, "NAOSDK needs Background Location access in order to" +
                        " provide a location when your device screen is turned off"));
            }
        }
    };

    public static boolean checkAndRequest(@NonNull final Activity activity,
                                          @NonNull final String[] permissions,
                                          String messagePermission,
                                          final int requestCode,
                                          DialogInterface.OnClickListener onCancelListener) {
        boolean result = false;

        // get the permissions to request
        List<String> permissionsToRequest = new ArrayList<>();
        boolean needExplanation = false;

        for (int i = 0; i < permissions.length; i++) {
            String permission = permissions[i];
            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsToRequest.add(permission);
                needExplanation = true;
            }
        }
        final String[] permissionsToRequestArray = permissionsToRequest.toArray(new String[permissionsToRequest.size()]);

        if (permissionsToRequestArray.length > 0) {
            if (needExplanation) {
                // Shows an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setMessage(messagePermission).setPositiveButton(activity.getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(activity, permissionsToRequestArray, requestCode);
                    }
                }).setNegativeButton(activity.getResources().getString(android.R.string.cancel), onCancelListener).show();
            } else {
                ActivityCompat.requestPermissions(activity, permissionsToRequestArray, requestCode);
            }
        }else{
            return true;
        }

        return result;
    }

    public static boolean checkAndRequest(@NonNull final Activity activity,
                                          @NonNull final List<String> permissions,
                                          DialogInterface.OnClickListener onCancelListener) {
        boolean result = false;
        for (final String permission : permissions) {
            final Pair<Integer, String> permRes = DEFAULT_PERMISSIONS.get(permission);
            // Here, thisActivity is the current mainView
            if (permRes != null && !activity.isFinishing() && ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {

                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder/*.setCancelable(false)*/.setMessage(permRes.second).setPositiveButton(activity.getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(activity, new String[]{permission}, permRes.first);
                        }
                    }).setNegativeButton(activity.getResources().getString(android.R.string.cancel), onCancelListener).show();

                } else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(activity, new String[]{permission}, permRes.first);

                    // MY_PERMISSIONS_REQUEST is an app-defined int constant.
                    // The callback method gets the result of the request.
                }
            } else {
                result = true;
            }
            break;
        }
        return result;
    }
}
