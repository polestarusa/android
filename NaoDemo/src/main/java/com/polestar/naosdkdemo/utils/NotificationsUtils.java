package com.polestar.naosdkdemo.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;

import androidx.core.app.NotificationCompat;

import java.util.List;

/**
 * Created by XLami on 05/10/2017.
 * This class aims to simplify notification creation because
 * they are new notification creation methods since Android_SDK >= 26
 */

public class NotificationsUtils {
    // a method for
    public static Notification createNotification(Context ctxt, // the context from which will be create th notification
                                                  String title, // the title of the notification
                                                  String message, // the message of the notification
                                                  int icon, // the icon of the notification
                                                  NotificationChannel channel, // the title notification channel
                                                  boolean autoCancel, // true if notification should autoCancel
                                                  PendingIntent pendingIntent, // the pendingIntent of the notification
                                                  List<NotificationAction> actionsList) { // a list of notification actions to create with NotificationAction class
        // create notification if Android_SDK >= 26
        if (Build.VERSION.SDK_INT >= 26) {
            Notification.Builder builder = new Notification.Builder(ctxt, channel.getId())
                    .setContentTitle(title)
                    .setSmallIcon(icon)
                    .setContentText(message)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(autoCancel);
            if (actionsList != null){
                for (NotificationAction action : actionsList) {
                    builder.addAction(new Notification.Action.Builder(action.actionIconId,
                            action.actionText, action.actionPI).build());
                }
            }
            return builder.build();
        // create notification if Android_SDK < 26
        } else {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(ctxt)
                    .setContentTitle(title)
                    .setSmallIcon(icon)
                    .setContentText(message)
                    .setContentIntent(pendingIntent)
                    .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                    .setAutoCancel(autoCancel);
            if(actionsList != null){
                for (NotificationAction action : actionsList) {
                    builder.addAction(new NotificationCompat.Action.Builder(action.actionIconId,
                            action.actionText, action.actionPI).build());
                }
            }
            return builder.build();
        }
    }

    // This class allows you to create a list of actions to associate with your notification
    public static class NotificationAction {
        private int actionIconId; // the id of the icon of the notification action
        private String actionText; // the text of the notification action
        private PendingIntent actionPI; // the pendingIntent of the notification action

        public NotificationAction(int IconId, String text, PendingIntent pendingIntent) {
            actionIconId = IconId;
            actionText = text;
            actionPI = pendingIntent;
        }
    }
}
